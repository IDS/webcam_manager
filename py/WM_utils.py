import PIL.Image
import io
import cv2
import shutil
import os
import glob
import toml

def WM_version():
    f = open("../pyproject.toml", "r")
    text = f.read()
    poetry = toml.loads(text)
    version = poetry['tool']['poetry']['version']
    return version

def camera_indexes():
    '''
    grab the available cam indexes
    checks the first 10 indexes.
    '''
    index = 0
    arr = []
    i = 10
    while i > 0:
        cap = cv2.VideoCapture(index)
        if cap.read()[0]:
            arr.append(index)
            cap.release()
        index += 1
        i -= 1
    return arr

def flushvideofiles():
    ''' Delete all video files accumulated from previous seshs'''
    dir_path = 'static'
    try:
        shutil.rmtree(dir_path)
        os.mkdir(dir_path)
    except OSError as e:
        print("Error: %s : %s" % (dir_path, e.strerror))

def isstaticfolderempty():
    '''check if the static/ folder already contains files'''
    if len(os.listdir('static/')) == 0:
        print("Directory is empty")
        filename = True
    else:
        print("Directory is not empty")
        list_of_files = glob.glob('static/*')  # * means all if need specific format then *.csv
        latest_file = max(list_of_files, key=os.path.getctime)
        filename = latest_file
    return filename

def resize(image,window_width = 500):
    '''
    Resize an image to the supplied width while preserving aspect ratio
    Adjusted (flipped) from here: https://stackoverflow.com/a/53631990
    '''
    aspect_ratio = float(image.shape[0])/float(image.shape[1])
    window_height = window_width*aspect_ratio
    image = cv2.resize(image, (int(window_width),int(window_height)))
    return image

def convert_to_bytes(file_or_bytes, resize=None):
    '''
    Copied from https://pysimplegui.readthedocs.io/en/latest/cookbook/

    Will convert into bytes and optionally resize an image that is a file or a base64 bytes object.
    Turns into  PNG format in the process so that can be displayed by tkinter
    :param file_or_bytes: either a string filename or a bytes base64 image object
    :type file_or_bytes:  (Union[str, bytes])
    :param resize:  optional new size
    :type resize: (Tuple[int, int] or None)
    :return: (bytes) a byte-string object
    :rtype: (bytes)
    '''
    if isinstance(file_or_bytes, str):
        img = PIL.Image.open(file_or_bytes)
    else:
        try:
            img = PIL.Image.open(io.BytesIO(base64.b64decode(file_or_bytes)))
        except Exception as e:
            dataBytesIO = io.BytesIO(file_or_bytes)
            img = PIL.Image.open(dataBytesIO)

    cur_width, cur_height = img.size
    if resize:
        new_width, new_height = resize
        scale = min(new_height/cur_height, new_width/cur_width)
        img = img.resize((int(cur_width*scale), int(cur_height*scale)), PIL.Image.ANTIALIAS)
    bio = io.BytesIO()
    img.save(bio, format="PNG")
    del img
    return bio.getvalue()