import pyvirtualcam
import cv2
import PySimpleGUI as sg
from imutils.video import count_frames
import os
import pathlib
import webbrowser

from WM_utils import *


# pyinstaller bundle maker, comment if not building binary
# command to execute:  pyinstaller --onefile --windowed --add-data "assets;assets" webcam_manager.py
# if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
#     os.chdir(sys._MEIPASS)


devices = camera_indexes()


# detect if we're on windows for default cam
if os.name == 'nt':
    cv_src = 0
else:
    # we are on linux
    cv_src = 0
    os.system("sudo modprobe v4l2loopback")
# TODO: handle macOS case

# set cv2 capture resolution
cwres = 1280
chres = 720

cv = cv2.VideoCapture(cv_src)
cv.set(cv2.CAP_PROP_FRAME_WIDTH, cwres)
cv.set(cv2.CAP_PROP_FRAME_HEIGHT, chres)

ret, frame = cv.read()

# pycam dimensions
cw = frame.shape[1]
ch = frame.shape[0]

cam = pyvirtualcam.Camera(width=cw, height=ch, fps=30, delay=0)

nativewidth = frame.shape[1]
nativeheight = frame.shape[0]

# recordings
filename = ""
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# fourcc = cv2.VideoWriter_fourcc(*'mp4v')
recordfeed = False

def about_window():
    '''
    About information
        - version
        - link to website/docs
    '''
    version = WM_version()
    layout = [
        [sg.Image('assets/app_icon_black.png')],
        [sg.Text("Webcam Manager", justification='center', font=("Steps Mono", 15), size=(200,1))],
        [sg.Text("Version: " + version, justification='center', size=(300,1))],
        [sg.Text("Documentation", size=(200,1), justification='center', enable_events=True, key="open_website")]
    ]
    window = sg.Window("About Webcam Manager", layout, modal=True, size=(260,220), element_justification='c', finalize=True)
    window.set_icon("assets/app_icon.ico")
    window['open_website'].update(font="Arial 10 underline")

    while True:
        event, values = window.read()
        if event == "open_website":
            webbrowser.open('https://diagram.institute')
        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        print(event, values)

    window.close()

def main():
    '''
    Main Window
    '''
    flushvideofiles()
    global cv
    recVidNum= 0
    recordfeed = False
    setLoop = False
    framecounter = 0
    frameoffset = 3
    devicenum = 0

    # create static/ folder if it doesn't exist
    if pathlib.Path("static").is_dir():
        print ("static already set")
    else:
        static_path = pathlib.Path("static")
        static_path.mkdir(parents=True, exist_ok=True)

    # check available cameras
    # devices = returnCameraIndexes()

    sg.theme('SystemDefault')
    # window layout
    layout = [
        [sg.Text("Webcam Manager",
            size=(25,1),
            font=("Steps-Mono", 20),
            justification="left"),
        sg.Button(
            image_data=convert_to_bytes('assets/button_ABOUT.png', (40, 40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            enable_events=True,
            key="About")
        ],
        [sg.Text("Device # ", size=(10,1), justification="left"),
            sg.Combo(devices, size=(10, 1), default_value=cv_src, key="DEVICE")
        ],
        [sg.Button(
            image_data=convert_to_bytes('assets/button_REC.png', (40, 40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            enable_events=True,
            #bind_space_key=True,
            key="REC"),
        sg.Button(
            image_data=convert_to_bytes('assets/button_STOP.png', (40, 40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            enable_events=True,
            key="STOP"),
        sg.Button(
            image_data=convert_to_bytes('assets/button_LOOP.png', (40, 40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            enable_events=True,
            bind_return_key=True,
            key="LOOP")
        ],
        [sg.Image(filename="", key="-IMAGE-")],
        [sg.Button(
            image_data=convert_to_bytes('assets/button_FLUSH.png', (40,40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            key="Flush"),
        sg.Button(
            image_data=convert_to_bytes('assets/button_EXIT.png', (40,40)),
            button_color=(sg.theme_background_color(), sg.theme_background_color()),
            border_width=0,
            key="Exit")
        ]
    ]

    # create window
    window = sg.Window("Webcam Manager", layout, size=(430,420))
    window.set_icon("assets/app_icon.ico")

    # Original colors
    REC_default_col = window["REC"].ButtonColor
    LOOP_default_col = window["LOOP"].ButtonColor
    Flush_default_col = window["Flush"].ButtonColor


    while True:
        event, values = window.read(timeout=20)

        device = values["DEVICE"]
        if device != devicenum:
            cv = cv2.VideoCapture(device)
            cv.set(cv2.CAP_PROP_FRAME_WIDTH, cwres)
            cv.set(cv2.CAP_PROP_FRAME_HEIGHT, chres)
            # print("--")


        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        elif event == "About":
            about_window();
        elif event == "Flush":
            flushvideofiles()
        elif event == "REC":
            if setLoop == True:
                print("looping cannot start recording")
            elif recordfeed == True:
                print("STOPPED RECORDING")
                window["REC"].Update(button_color=REC_default_col, disabled=False)
                window["LOOP"].Update(button_color=LOOP_default_col, disabled=False)
                window["Flush"].Update(button_color=Flush_default_col, disabled=False)
                recordfeed = False
                out.release()
            else:
                print("RECORDING")
                filename="static/output_"
                filename += str(recVidNum)
                filename += ".avi"
                recVidNum += 1
                out = cv2.VideoWriter(filename, fourcc, 20.0, (nativewidth, nativeheight))
                recordfeed = True
                window["REC"].Update(button_color=('white', 'red'))
                window["LOOP"].Update(button_color=('black', 'grey'), disabled=True)
                window["Flush"].Update(button_color=('black', 'grey'), disabled=True)
        elif event == "STOP":
            print("STOPPED RECORDING/LOOPING")
            window["REC"].Update(button_color=REC_default_col, disabled=False)
            window["LOOP"].Update(button_color=LOOP_default_col, disabled=False)
            window["Flush"].Update(button_color=Flush_default_col, disabled=False)
            if recordfeed == True:
                recordfeed = False
                out.release()
            elif setLoop == True:
                setLoop = False
                cv = cv2.VideoCapture(device)
        elif event == "LOOP":
            print("LOOPING")
            if isstaticfolderempty() == True:
                print("no videos to loop")
                window["LOOP"].Update(button_color=('black', 'grey'), disabled=True)
            else:
                if recordfeed == True:
                    print("currently recording cannot loop")
                elif setLoop == True:
                    print("STOPPED LOOPING")
                    window["REC"].Update(button_color=REC_default_col, disabled=False)
                    window["LOOP"].Update(button_color=LOOP_default_col, disabled=False)
                    window["Flush"].Update(button_color=Flush_default_col, disabled=False)
                    setLoop = False
                    cv = cv2.VideoCapture(device)
                else:
                    filename = isstaticfolderempty()
                    setLoop = True
                    cv = cv2.VideoCapture(filename)
                    framecount = count_frames(filename)
                    window["LOOP"].Update(button_color=('black', 'yellow'))
                    window["REC"].Update(button_color=('black', 'grey'), disabled=True)
                    window["Flush"].Update(button_color=('black', 'grey'), disabled=True)

        success, frame = cv.read()

        if recordfeed == True:
            out.write(frame)

        if setLoop == True:
            framecounter += 1
            if framecounter >= framecount - frameoffset:
                framecounter = 0
                cv.set(cv2.CAP_PROP_POS_FRAMES, 0)
                print("starting loop again")

        scaled_preview = resize(frame, window_width=400)
        imgbytes = cv2.imencode(".png", scaled_preview)[1].tobytes()
        window["-IMAGE-"].update(data=imgbytes)
        #window["-IMAGE-"].set_size(size=(vid_preview_w, vid_preview_h))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)

        if frame.shape[0] != cam.height:
            frame = cv2.resize(frame, (cw, ch))
            frame = frame.reshape(ch,cw,4)
        try:
            cam.send(frame)
        except:
            pass
        cam.sleep_until_next_frame()
        devicenum = device
    window.close()


main()
