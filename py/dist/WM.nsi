!include "FontRegAdv.nsh"
!include "FontName.nsh"

;NSIS Macros




!define PRODUCT_NAME           "Webcam Manager"
!define EXE_NAME               "webcam_manager.exe"
!define PRODUCT_VERSION        "Alpha 0.1"
!define PRODUCT_PUBLISHER      "Institute of Diagram Studies"
!define PRODUCT_LEGAL          "AGPL-3"
!define TEMP_DIR               ""

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file
  Name "Webcam Manager"
  OutFile "WebcamManager_installer.exe"
  ;Unicode True

  ;Default installation folder
  InstallDir "$PROGRAMFILES\Webcam_Manager"

  ;Get installation folder from registry if available
  ;InstallDirRegKey HKCU "Software\Modern UI Test" ""

  ;Request application privileges for Windows Vista
  ;RequestExecutionLevel user

  !define MUI_ICON "inc\logo.ico"
  !define MUI_FINISHPAGE_RUN "$INSTDIR\webcam_manager.exe"

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "inc\License.txt"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH


  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Fonts" fonts
  StrCpy $FONT_DIR $FONTS
  !insertmacro InstallTTF "inc\Steps-Mono.ttf"
  SendMessage ${HWND_BROADCAST} ${WM_FONTCHANGE} 0 0 /TIMEOUT=5000
SectionEnd

Section "OBS Studio" Prerequisites

  SetOutPath "$INSTDIR"

  MessageBox MB_YESNO "Install OBS?" /SD IDYES IDNO endOBS
    File "inc\OBS-Studio-26.1.1-Full-Installer-x64.exe"
    ExecWait "$INSTDIR\OBS-Studio-26.1.1-Full-Installer-x64.exe"
    Goto endOBS
  endOBS:
SectionEnd

Section "Webcam Manager" WebMan

  ;ADD YOUR OWN FILES HERE...
  File "webcam_manager.exe"

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_fonts ${LANG_ENGLISH} "Necessary GUI Fonts"
  LangString DESC_Prerequisites ${LANG_ENGLISH} "Relies on OBS virtual camera."
  LangString DESC_WebMan ${LANG_ENGLISH} "Manage your webcam"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${fonts} $(DESC_fonts)
    !insertmacro MUI_DESCRIPTION_TEXT ${Prerequisites} $(DESC_Prerequisites)
    !insertmacro MUI_DESCRIPTION_TEXT ${WebMan} $(DESC_WebMan)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\webcam_manager.exe"

  RMDir "$INSTDIR"

  DeleteRegKey /ifempty HKCU "Software\Modern UI Test"

SectionEnd