# Webcam Manager 

A simple program to loop your webcam on video calls.

[toc]

Features:

- Virtual webcam compatible with all video-call software
- Record loops and play them back as if they were from your webcam
- Generic name and interface to avoid scrutiny from co-workers and/or desktop monitoring



![webcam manager banner](images/banner_02.png)

## Installation 

### Windows

On Windows *Webcam Manager* relies on [OBS Studio](https://obsproject.com/) to create a virtual webcam that can be used in video-call software.

To install both *Webcam Manager* and OBS Studio, use the [installer](py/dist/WebcamManager_installer.exe)

If OBS is already installed, you can use [webcam_manager.exe](py/dist/webcam_manager.exe)

### MacOS and Linux 

Binaries and/or packages are not yet available. Please install using git and python as described below in [Development Installation](#development-installation).


## Operation

*Webcam Manager* is designed to be used on devices that may be monitored, for example through desktop monitoring (screenshots taken at random intervals).

For this reason nothing in the program window explicitly refers to looping the webcam. The application window looks as generic as possible. The few buttons are presented with no text and only icons usually associated with camera focus. This means that *Webcam Manager* may take some practice to operate. Fortunately, recording and playing webcam loops are the only functions available so the program is easy to learn. 

![User-Interface Guide](images/UI-guide.png)  

### Connect to call software 

1. Launch *Webcam Manager*
2. Ensure the webcam is showing in the preview
3. Launch the video-call software
4. Select `Dummy video device` as a camera 
   - In browser-based video-calls (Jitsi, Google Meet, Zoom) this will be associated with the dialog asking for "permission" to use the camera. See example below in Jitsi and Firefox.
   - In applications (Zoom, Microsoft Teams, etc) this is more likely to be found in video settings.

![example camera source dialog in Jitsi and Firefox with "Dummy video device" selected.](images/video_device.png)

### Recording a loop

1. Place your mouse cursor over the REC button
2. Click to start recording 
3. Keep a static pose moving as little as possible
4. Click again to stop recording 

### Playing a loop 

Once a loop has been recorded.

1. Click the LOOP button or press the `Enter` key 
2. The loop button is yellow when a loop is playing 
3. Click the STOP button or press `Enter` to stop and display the live webcam again.

### Freezing the camera 

To make the transition from loop back to a live camera, it can be useful to "freeze" the image and blame it on the connection.

Opening the About window with the **?** button freezes the picture for as long as the window is open.


## Development installation

1. Clone this repository

```bash
git clone git@gitlab.com:davidbenque/webcam_manager.git
cd webcam_manager
```

2. Install dependencies

    If using poetry:

    ```bash
    poetry install
    ``` 

    else install the dependencies listed in `requirements.txt`

3. Virtual webcam dependencies  
   
    Required to create the virtual webcam that can be used in call software.

    - On Windows [OBS Studio](https://obsproject.com/) should be installed.

    - On Linux [v4l2loopback](https://github.com/umlaeute/v4l2loopback) is required, to install: 
    
      ```bash
      sudo apt install v4l2loopback-dkms
      ```
    
        v4l2loopback should be running before *Webcam Manager* is launched. If it not the program will launch it and ask for a sudo password.

        To launch v4l2loopback manually:  

      ``` bash
      sudo modprobe v4l2loopback devices=1
      ```
4. Launch 


### How to build Windows binaries

To build a binary from the `webcam_manager.py` file, uncomment line 14 and 15:

```python
if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
    os.chdir(sys._MEIPASS)
```

then use `pyinstaller` to execute the following command:

```bash
pyinstaller --onefile --windowed --add-data "assets;assets" webcam_manager.py
```

This will bundle all files and dependencies in a single file (`--onefile`). Any assets and external files such as images are included using `--add-data` flag followed with the directory or file name (`"assets;assets"`) to be included within the build.

This will create an `.exe` file in the `py/dist` directory along a `py/build` directory and a `webcam_manager.spec` file.
 
#### Distribution / Create an installer for windows

In order to create an installer that bundles OBS and Webcam Manager, [OBS Studio installer](https://cdn-fastly.obsproject.com/downloads/OBS-Studio-26.1.1-Full-Installer-x64.exe) is required.

Once downloaded move the installer in the `py/dist/inc` directory. This folder contains the external files bundled with your webcam application, such as the `logo.ico` and the `License.txt`.

We used [NSIS](https://nsis.sourceforge.io/Download) to create the installer; once you have NSIS installed, built the `py/dist/webcam_manager.exe` file as well as the `py/dist/inc/OBS-Studio-26.1.1-Full-Installer-x64.exe` file, compile the `py/dist/WM.nsi` with NSIS.

This should generate the `py/dist/WebcamManager_installer.exe`.

