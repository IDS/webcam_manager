
# TODO

- app 
  - [x] about window
  - [ ] remove borders around windows
- [ ] Readme


- [x] app/installer icon

- [ ] V4L2 warnings on Linux 
  - 9 warnings so I guess one for each camera that is not found (we test for 10 devices in `camera_indexes()`)

- [ ] Crash when bad value (e.g. nothing) is entered manually in the device dropdown.


- [x] Start modprobe service automatically from Python? 
  - probably can call a `subprocess` or something. Probably check first if the `v4l2loopback` package exist, and if not display the command to execute? 
  - asks for sudo password.

- [ ] Package Font?
  - Title using [Steps Mono](https://www.velvetyne.fr/fonts/steps-mono/download/) libre font from Velvetyne.
  - Can we embed it/package it? 
  - let's see how it goes when we use `pyinstaller`. As far as I understand `pysimplegui` uses `tkinter` to manage fonts, so checking how tkinter does it might give us a clue.
    - https://stackoverflow.com/questions/63585632/how-to-add-a-truetype-font-file-to-a-pyinstaller-executable-for-use-with-pygame

- [ ] How do we package the rest anyway?
  - Cookiecutter: https://cookiecutter.readthedocs.io/en/1.7.2/index.html

  - package FFMPEG builds and include in pyinstaller
https://stackoverflow.com/a/57081121
https://stackoverflow.com/a/60822647

## DONE

- [x] Stealth button names/colors
  - icons only? see [this](https://pysimplegui.readthedocs.io/en/latest/cookbook/#use-with-buttons)

- [x] Keyboard shortcuts
  - ~~to easily start/stop recording: space bar~~
  - ~~start/stop loop: enter?~~
  - Done and tested on Windows!!

- [x] Added device selector for the camera, to be tested elsewhere

- [x] Added a flush function 
  - Empties the video recorded in static at the start of the application
  - Added a `Flush` button next the `Exit`

- [x] Pick normcore theme

- [x] create `static/` if it doesn't exist.
  
- [x] Output of virtualcam (e.g. previewing in Jitsi) has weird smurf colors.
  - ~~preview in window is fine~~ 
  - ~~recorded output file was fine~~
  - preview in window fine, recorded out fine, preview in Zoom fine.
  
- [x] Are we downsizing the video resolution? why not use best res available? (line 57 see # XXX)
  - we was, now we're webcam native size
  
- [x] Click on LOOP after launch before recording crashes
  - ~~`variable referenced before assignment`~~
  - fixed. If you click LOOP first and there's no static videos, the button gets disabled. Also added colours and disabled buttons

- [x] Stop saving video files locally?
  - or at least add to `.gitignore`? 
